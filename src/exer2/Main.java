package exer2;

public class Main {
	public static void main(String[] args) {
		// wsimport -keep -p NOME LINK_DO_WSDL
		CalculadoraImpService service = new CalculadoraImpService();
		Calculadora proxy = service.getCalculadoraImpPort();

		System.out.println(proxy.somar(10, 10));
		System.out.println(proxy.somar(50, 10));
	}
}

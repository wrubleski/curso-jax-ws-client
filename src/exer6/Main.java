package exer6;

import java.math.BigDecimal;

public class Main {
	public static void main(String[] args) {
		VisaEletron servico = new VisaEletron(); // service name

		CartaoVisa proxi = servico.getCartaoVisaImpPort(); // portType
		Consulta consulta = proxi.operacaoDebitarSaldo(new BigDecimal(10)); // operation
		System.out.println(consulta.getMensagem());

		proxi.operacaoCreditarSaldo(new BigDecimal(5));

		consulta = proxi.operacaoDebitarSaldo(new BigDecimal(10)); // operation
		System.out.println(consulta.getMensagem());

	}
}

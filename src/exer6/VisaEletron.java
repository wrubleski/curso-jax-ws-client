
package exer6;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "VisaEletron", targetNamespace = "http://visa.eletron.consulta", wsdlLocation = "http://127.0.0.1:9090/visa?wsdl")
public class VisaEletron
    extends Service
{

    private final static URL VISAELETRON_WSDL_LOCATION;
    private final static WebServiceException VISAELETRON_EXCEPTION;
    private final static QName VISAELETRON_QNAME = new QName("http://visa.eletron.consulta", "VisaEletron");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://127.0.0.1:9090/visa?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        VISAELETRON_WSDL_LOCATION = url;
        VISAELETRON_EXCEPTION = e;
    }

    public VisaEletron() {
        super(__getWsdlLocation(), VISAELETRON_QNAME);
    }

    public VisaEletron(WebServiceFeature... features) {
        super(__getWsdlLocation(), VISAELETRON_QNAME, features);
    }

    public VisaEletron(URL wsdlLocation) {
        super(wsdlLocation, VISAELETRON_QNAME);
    }

    public VisaEletron(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, VISAELETRON_QNAME, features);
    }

    public VisaEletron(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public VisaEletron(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns CartaoVisa
     */
    @WebEndpoint(name = "CartaoVisaImpPort")
    public CartaoVisa getCartaoVisaImpPort() {
        return super.getPort(new QName("http://visa.eletron.consulta", "CartaoVisaImpPort"), CartaoVisa.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns CartaoVisa
     */
    @WebEndpoint(name = "CartaoVisaImpPort")
    public CartaoVisa getCartaoVisaImpPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://visa.eletron.consulta", "CartaoVisaImpPort"), CartaoVisa.class, features);
    }

    private static URL __getWsdlLocation() {
        if (VISAELETRON_EXCEPTION!= null) {
            throw VISAELETRON_EXCEPTION;
        }
        return VISAELETRON_WSDL_LOCATION;
    }

}


package exer6;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for operacaoDebitarSaldoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="operacaoDebitarSaldoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resultadoConsultaDeSaldo" type="{http://visa.eletron.consulta}consulta" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "operacaoDebitarSaldoResponse", propOrder = {
    "resultadoConsultaDeSaldo"
})
public class OperacaoDebitarSaldoResponse {

    protected Consulta resultadoConsultaDeSaldo;

    /**
     * Gets the value of the resultadoConsultaDeSaldo property.
     * 
     * @return
     *     possible object is
     *     {@link Consulta }
     *     
     */
    public Consulta getResultadoConsultaDeSaldo() {
        return resultadoConsultaDeSaldo;
    }

    /**
     * Sets the value of the resultadoConsultaDeSaldo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Consulta }
     *     
     */
    public void setResultadoConsultaDeSaldo(Consulta value) {
        this.resultadoConsultaDeSaldo = value;
    }

}

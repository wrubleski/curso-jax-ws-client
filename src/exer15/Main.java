package exer15;

import java.math.BigDecimal;

public class Main {
	public static void main(String[] args) {
		VendaImpService servico = new VendaImpService();
		Venda proxy = servico.getVendaImpPort();

		Produto p = new Produto();

		try {
			proxy.vender(p);
		} catch (VendaException_Exception e) {
			System.out.println(e.getMessage());
		}

		p.setNome("");

		try {
			proxy.vender(p);
		} catch (VendaException_Exception e) {
			System.out.println(e.getMessage());
		}

		p.setNome(null);

		try {
			proxy.vender(p);
		} catch (VendaException_Exception e) {
			System.out.println(e.getMessage());
		}

		p.setNome("Luiz");

		try {
			proxy.vender(p);
		} catch (VendaException_Exception e) {
			System.out.println(e.getMessage());
		}

		p.setValor(BigDecimal.ZERO);

		try {
			proxy.vender(p);
		} catch (VendaException_Exception e) {
			System.out.println(e.getMessage());
		}

		p.setValor(BigDecimal.TEN);

		try {
			proxy.vender(p);
		} catch (VendaException_Exception e) {
			System.out.println(e.getMessage());
		}

	}
}


package exer8;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "DataHora", targetNamespace = "http://exer7/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface DataHora {


    /**
     * 
     * @return
     *     returns java.lang.String
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "get", targetNamespace = "http://exer7/", className = "exer8.Get")
    @ResponseWrapper(localName = "getResponse", targetNamespace = "http://exer7/", className = "exer8.GetResponse")
    @Action(input = "http://exer7/DataHora/getRequest", output = "http://exer7/DataHora/getResponse")
    public String get();

}

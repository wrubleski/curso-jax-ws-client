package exer4;

import java.util.List;

public class Main {
	public static void main(String[] args) {
		LivrariaOnLine servico = new LivrariaOnLine();
		Livraria proxy = servico.getLivrariaImpPort();
		List<Livro> estoque = proxy.getEstoque();
		estoque.forEach(l -> {
			System.out.println(l.getId() + " - " + l.getNome());
		});
	}
}

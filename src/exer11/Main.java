package exer11;

public class Main {
	public static void main(String[] args) {
		ContatoImpService servico = new ContatoImpService();
		servico.setHandlerResolver(new Configurador());
		Telefone proxy = servico.getContatoImpPort();

		Contato c = new Contato();
		c.setNome("Luiz Henrique");
		c.setFone("123456789");

		proxy.adicionar(c);

		c = new Contato();
		c.setNome("Usuario Dois");
		c.setFone("987654321");

		proxy.adicionar(c);

		proxy.getContatos().forEach(contato -> System.out.println(contato.getNome()));

		proxy.deletar(c);

		proxy.getContatos().forEach(contato -> System.out.println(contato.getNome()));

	}
}

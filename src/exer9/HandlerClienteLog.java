package exer9;

import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

public class HandlerClienteLog implements SOAPHandler<SOAPMessageContext> {

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		Boolean saida = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		if (saida) {
			System.out.println("LOG - Saindo mess soap");
			log(context);
		} else {
			System.out.println("LOG - Chegando mess soap");
			log(context);
		}

		return true;
	}

	private void log(SOAPMessageContext context) {
		try {
			context.getMessage().writeTo(System.out);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {

		return false;
	}

	@Override
	public void close(MessageContext context) {

	}

	@Override
	public Set<QName> getHeaders() {

		return null;
	}

}
